/*	1. Write a program that allows storing graph in adjacency matrix
	2. The program should contain a function listing number of vertices and edges
	3. The program should give the user opportunity to write number of edges going from a particular 
	vertex - the user gives the number of vertices as an input in console. */

package lab11;
import java.util.*;

/***************************************NOTICE***************************************/
/**This program works only in case the input vertices are integers, counting from 1**/
/*******For example: graph with 3 vertices then the vertices will be 1, 2, 3*********/

public class AdjMatrix {
	private int vertices;
	private int[][] adjacency_matrix;
	
	public static int v; //vertices 
	public static int a; //determine if the graph is directed or not
	public static AdjMatrix graph;

	// constructor
	public AdjMatrix(int v) {
		this.vertices = v;
		adjacency_matrix = new int[vertices + 1][vertices + 1];
	}

	//method creating an edge from one vertex to one vertex
	public void makeEdge(int from, int to) {
		try {
			adjacency_matrix[from][to] = 1;
			if (a == 0) //if the graph is undirected, edge from A to B equal to from B to A
				adjacency_matrix[to][from] = 1; 
		} catch (ArrayIndexOutOfBoundsException index) {
			System.out.println("The vertices does not exists");
		}
	}
	
	//method getting the edge between two specific vertices
	public int getEdge(int to, int from) {
		try {
			return adjacency_matrix[to][from];
		} catch (ArrayIndexOutOfBoundsException index) {
			System.out.println("The vertices does not exists");
		}
		return -1;
	}
	
	//method printing the adjacency matrix representing the graph
	public static void printGraph() {
		System.out.println("The adjacency matrix for the given graph is: ");
		System.out.print("  ");
		for (int i = 1; i <= v; i++)
			System.out.print(i + " ");
		System.out.println();		
		for (int i = 1; i <= v; i++) {
			System.out.print(i + " ");
			for (int j = 1; j <= v; j++)				
				System.out.print(graph.getEdge(i, j) + " ");
			System.out.println();
		}
	}

	public static void main(String args[]) {	
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of vertices: ");
		v = input.nextInt();
		System.out.println("Is the graph directed? (type 1 for yes or 0 for no)");
		a = input.nextInt();
		
		graph = new AdjMatrix(v);
		int count = 0; //count number of edges
		
		System.out.println("Enter the edges: <from> <to> (type f to finish input)");
		while (true) {
			try {
				int from = input.nextInt();
				int to = input.nextInt();
				graph.makeEdge(from, to);
				count++;
			} catch (InputMismatchException e) {
				input.close();
				System.out.println("Number of edges: " + count);
				printGraph();
				System.exit(0);
			}
		}
	}
}
