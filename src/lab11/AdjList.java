/*	1. Write a program that allows storing graph in adjacency list
	2. The program should contain a function listing number of vertices and edges
	3. The program should give the user opportunity to write number of edges going from a particular 
	vertex - the user gives the number of vertices as an input in console. */

package lab11;
import java.util.*;

/***************************************NOTICE***************************************/
/**This program works only in case the input vertices are integers, counting from 0**/
/*******For example: graph with 3 vertices then the vertices will be 0, 1, 2*********/

public class AdjList {
	static class Graph {
		int vertices;
		LinkedList<Integer> adjListArray[];
		public static int a; //determine if the graph is directed or not

		// constructor
		Graph(int V) {
			this.vertices = V;			
			adjListArray = new LinkedList[V]; // define the size of array as number of vertices		
			for (int i = 0; i < V; i++) { // Create a new list for each vertex to store adjacent nodes
				adjListArray[i] = new LinkedList<>(); 
			}
		}
	}

	// method creating an edge from one vertex to one vertex
	static void makeEdge(Graph graph, int from, int to) {
		graph.adjListArray[from].add(to);
		if (Graph.a == 0) // if the graph is undirected, edge from A to B equal to from B to A
			graph.adjListArray[to].add(from);
	}

	// method printing the adjacency list representing the graph
	static void printGraph(Graph graph) {
		for (int v = 0; v < graph.vertices; v++) {
			System.out.print("Adjacency list of vertex " + v + ":");
			for (Integer pCrawl : graph.adjListArray[v]) {
				System.out.print(" -> " + pCrawl);
			}
			System.out.println("\n");
		}
	}

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of vertices: ");
		int V = input.nextInt();
		System.out.println("Is the graph directed? (type 1 for yes or 0 for no)");
		Graph.a = input.nextInt();

		Graph graph = new Graph(V);
		int count = 0; //count number of edges

		System.out.println("Enter the edges: <from> <to> (type f to finish input)");
		while (true) {
			try {
				int from = input.nextInt();
				int to = input.nextInt();
				makeEdge(graph, from, to);
				count++;
			} catch (InputMismatchException e) {
				input.close();
				System.out.println("Number of edges: " + count);
				printGraph(graph);
				System.exit(0);
			}
		}
	}
}
